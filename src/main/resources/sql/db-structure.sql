/*DROP  TABLE VILLAGE;
DROP TABLE NEWS;
DROP TABLE SERVICES;
DROP TABLE EVENT;
*/
USE VS;
DROP TABLE VILLAGE_VILLAGE_EVENTS;
DROP TABLE VILLAGE_NEWS;
DROP TABLE VILLAGE_SERVICES;
DROP TABLE VILLAGE_EVENTS;

DROP  TABLE VILLAGE;


CREATE TABLE VILLAGE(
V_ID INT(10) NOT NULL AUTO_INCREMENT,
V_NAME VARCHAR(15) NOT NULL,
V_DIST VARCHAR(15) NOT NULL,
V_STATE VARCHAR(15) NOT NULL,
V_POP INT NOT NULL,
V_PINCODE INT(10) NOT NULL,
V_INFO VARCHAR(3000) NOT NULL,
PRIMARY KEY(V_ID));


CREATE TABLE VILLAGE_SERVICES(
S_ID  INT(10) NOT NULL AUTO_INCREMENT,
V_ID INT(10) NOT NULL,
S_NAME VARCHAR(15) NOT NULL,
S_INFO VARCHAR(2000) NOT NULL, 
PRIMARY KEY(S_ID));

CREATE TABLE VILLAGE_NEWS(
V_ID INT(10) NOT NULL,
N_ID INT(10) NOT NULL AUTO_INCREMENT,
N_NAME VARCHAR(15) NOT NULL,
N_INFO VARCHAR(1000) NOT NULL,
PRIMARY KEY(N_ID));


CREATE TABLE VILLAGE_EVENTS(
V_ID INT(10) NOT NULL,
E_ID INT(10) NOT NULL AUTO_INCREMENT,
E_NAME VARCHAR(10) NOT NULL,
E_INFO VARCHAR(1000) NOT NULL,
PRIMARY KEY(E_ID));

ALTER TABLE VILLAGE_SERVICES ADD CONSTRAINT FK_VILLAGE_SERVICES_VID FOREIGN KEY  (V_ID) REFERENCES VILLAGE(V_ID);
ALTER TABLE VILLAGE_NEWS ADD CONSTRAINT FK_VILLAGE_NEWS_VID FOREIGN KEY  (V_ID) REFERENCES VILLAGE(V_ID);
ALTER TABLE VILLAGE_EVENTS ADD CONSTRAINT FK_VILLAGE_EVENTS_VID FOREIGN KEY  (V_ID) REFERENCES VILLAGE(V_ID);


INSERT INTO VILLAGE (V_ID, V_NAME, V_DIST, V_STATE,V_POP,V_PINCODE, V_INFO) VALUES (1, 'SAMARKHA','ANAND','ANAND',200000, 388220, 'According to Census 2011 information the location code or village code of Samarkha village is 516913. Samarkha village is located in Anand Tehsil of Anand district in Gujarat, India. It is situated 6km away from Anand, which is both district & sub-district headquarter of Samarkha village. As per 2009 stats, Samarkha village is also a gram panchayat.

The total geographical area of village is 2127.07 hectares. Samarkha has a total population of 22,712 peoples. There are about 4,612 houses in Samarkha village. As per 2019 stats, Samarkha villages comes under Umreth assembly & Anand parliamentary constituency. Anand is nearest town to Samarkha which is approximately 6km away.');
INSERT INTO VILLAGE (V_ID, V_NAME, V_DIST, V_STATE,V_POP,V_PINCODE, V_INFO) VALUES (2, 'SOJITRA','ANAND','ANAND',200000, 388221, 'Sojitra is a village in the Indian state of Gujarat. It is in the Anand district, situated at 22°33′N 72°43′E. Its nearest villages are Isnav (4 km away), Dabhou (6 km away), Gada (4 km), Devataj (2 km), and Limbali (3 km). The village is home to several schools, including Smt. H.J. Patel Primary School for Girls, inaugurated by the Governor of Gujarat in 2006 and The M.M. High School and Library. Sojitra also has a number of temples dedicated to different deities of Hindu and Jain faiths.[1] Sojitra is place of visa mevada Digamber jain panch. Once it was a town village located nearby Khambhat. When Khambhat was a major port on the confluence of Mahi Sagar, highways emerging from Khambhat passed through Sojitra to far away hinter lands. This had made Sojitra an important industrial center. It was the hub of businessmen, money lenders and famous artisans. The ponds (kund), step well, Jain temples, Buddha statues, Masjid of the village still provide a glimpse of its glorious past and antiquity. The statues found during the excavation of the pond traces the existence of the village as far back as second century BC according to the archaeologists.');


INSERT INTO VILLAGE_EVENTS(V_ID, E_ID, E_NAME, E_INFO) VALUES(1,1,'EVENT 1','TEST INFO');
