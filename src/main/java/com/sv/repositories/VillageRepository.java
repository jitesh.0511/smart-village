package com.sv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sv.entities.Village;

@Repository
public interface VillageRepository extends JpaRepository<Village, Integer> {
	@Query("select v from Village v where upper(v.vDist) = upper(?1) and upper(v.vName)= upper(?2) ")
	Village findVillage(String districtName, String villageName);

}
