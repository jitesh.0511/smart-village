package com.sv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartVillageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartVillageApplication.class, args);
	}

}
