package com.sv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.sv.entities.Village;
import com.sv.repositories.VillageRepository;

@Controller
public class HomePageController {
	@Autowired
	VillageRepository villageRepository;

	@GetMapping("/")
	public ModelAndView homePage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		List<Village> villages = villageRepository.findAll();
		mv.addObject("villages", villages);
		return mv;
	}

	@GetMapping("/village/{districtName}/{villageName}/about")
	public ModelAndView getVillageHomePage(@PathVariable("districtName") String districtName,
			@PathVariable("villageName") String villageName) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("about");
		Village village = villageRepository.findVillage(districtName, villageName);
		mv.addObject("village", village);
		return mv;
	}

	@GetMapping("/village/{districtName}/{villageName}/events")
	public ModelAndView getVillageEvents(@PathVariable("districtName") String districtName,
			@PathVariable("villageName") String villageName) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("events");
		Village village = villageRepository.findVillage(districtName, villageName);
		mv.addObject("village", village);
		return mv;
	}
	
	@GetMapping("/village/{districtName}/{villageName}/services")
	public ModelAndView getVillageServices(@PathVariable("districtName") String districtName,
			@PathVariable("villageName") String villageName) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("events");
		Village village = villageRepository.findVillage(districtName, villageName);
		mv.addObject("village", village);
		return mv;
	}

}
