package com.sv.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "VILLAGE")
public class Village {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "V_ID")
	private Integer vId;
	@Column(name = "V_NAME")
	private String vName;
	@Column(name = "v_dist")
	private String vDist;
	@Column(name = "V_STATE")
	private String vState;
	@Column(name = "V_POP")
	private Integer vPop;
	@Column(name = "V_PINCODE")
	private Integer vPincode;
	@Column(name = "V_INFO")
	private String vInfo;
	@OneToMany(mappedBy = "vId")
	List<VillageEvents> villageEvents;

	public Integer getvId() {
		return vId;
	}

	public void setvId(Integer vId) {
		this.vId = vId;
	}

	public String getvName() {
		return vName;
	}

	public void setvName(String vName) {
		this.vName = vName;
	}

	public String getvDist() {
		return vDist;
	}

	public void setvDist(String vDist) {
		this.vDist = vDist;
	}

	public String getvState() {
		return vState;
	}

	public void setvState(String vState) {
		this.vState = vState;
	}

	public Integer getvPop() {
		return vPop;
	}

	public void setvPop(Integer vPop) {
		this.vPop = vPop;
	}

	public String getvInfo() {
		return vInfo;
	}

	public void setvInfo(String vInfo) {
		this.vInfo = vInfo;
	}

	public List<VillageEvents> getVillageEvents() {
		return villageEvents;
	}

	public void setVillageEvents(List<VillageEvents> villageEvents) {
		this.villageEvents = villageEvents;
	}

	public Integer getvPincode() {
		return vPincode;
	}

	public void setvPincode(Integer vPincode) {
		this.vPincode = vPincode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vId == null) ? 0 : vId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Village other = (Village) obj;
		if (vId == null) {
			if (other.vId != null)
				return false;
		} else if (!vId.equals(other.vId))
			return false;
		return true;
	}

}
