package com.sv.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "VILLAGE_EVENTS")
public class VillageEvents {

	@ManyToOne
	@JoinColumn(name = "V_ID", nullable = false)
	private Village vId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "E_ID")
	private Integer eId;
	@Column(name = "E_NAME")
	private String eName;
	@Column(name = "E_INFO")
	private String eInfo;

	public Village getvId() {
		return vId;
	}

	public void setvId(Village vId) {
		this.vId = vId;
	}

	public Integer geteId() {
		return eId;
	}

	public void seteId(Integer eId) {
		this.eId = eId;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public String geteInfo() {
		return eInfo;
	}

	public void seteInfo(String eInfo) {
		this.eInfo = eInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eId == null) ? 0 : eId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VillageEvents other = (VillageEvents) obj;
		if (eId == null) {
			if (other.eId != null)
				return false;
		} else if (!eId.equals(other.eId))
			return false;
		return true;
	}

}
